<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

<style>
	.wrpper{
		margin: 0 auto;
		width: 600px;

		
	}
	#ajaxwrap, #jqueryWrap{
		height: 300px;
		overflow-y: scroll;
		border: 2px solid #ddd;
		margin-bottom: 20px;
		padding: 10px 30px;
	}
</style>	




<div class="wrpper">

	<!-- Xml Ajax json data-->
	<div id="ajaxwrap">
		<div class="box"></div>
	</div>



	<!-- jQuery ajax json data-->
	<div id="jqueryWrap">
		<div class="box"></div>
	</div>


</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
	
	var request = new XMLHttpRequest(); 
	var url = 'https://jsonplaceholder.typicode.com/posts';

	request.open('GET', url , true);

	// console.log(request.readyState);

	request.send();
	 
	request.onreadystatechange = function handleRequest(){

	   	if(request.readyState === 4 && request.status === 200) {
			var data = JSON.parse(request.responseText);

			showArtists(data);

	    }
	}

	function showArtists(data) {

		var out = "";

		data.forEach(function(entry) {
			out += '<div id="box">';
			out += '<h2>' + entry.title +' </h2>';
			out += '<p>' + entry.body + '<p>';
			out += '</div>';
		});

		document.getElementById("wrap").innerHTML = out;
	}

</script>





<script type="text/javascript">
	$(document).ready(function(){

		var main = $(".box");

		$.ajax({
			// url: 'https://learnwebcode.github.io/json-example/animals-2.json',
			url: 'https://jsonplaceholder.typicode.com/posts',
			contentType: "application/json",
			type: 'GET',
			dataType: 'json',
	       	success: function(response, statusText, xhr){


	           if (statusText =='success') {

	           		var output = "";

					$.each(response, function(index, value){

						output += '<div class="box"><h2>' + value.title + '</h2><p>' + value.body + '</p></div>';

					});

					// Or
				   // response.forEach(function(data) {
				   //    $(".messages").append("<li>" + data.title + ":" + data.body + "</li>");
				   // });

					main.append(output);

	           }
	           else{
	               alert("No found");
	           }
	       	},

		    error: function (xhr, status, errorThrown) {

				var msg = '';

				if (xhr.status === 0) {
				    msg = 'Not connect.\n Verify Network.';
				} else if (xhr.status == 404) {
				    msg = 'Requested page not found. ['+xhr.status+']';
				} else if (xhr.status == 500) {
				    msg = 'Internal Server Error ['+xhr.status+'].';
				} else if (exception === 'parsererror') {
				    msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
				    msg = 'Time out error.';
				} else if (exception === 'abort') {
				    msg = 'Ajax request aborted.';
				} else {
				    msg = 'Uncaught Error.\n' + xhr.responseText;
				}


		    }

		});
		
	   
	});
</script>





</body>
</html>
